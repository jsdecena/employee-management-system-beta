@extends('admin.tpl.master')

@section('title')
  Users List - Employee Management and Leave System
@stop

@section('content')
	
	<div class="page-header">
		<h1>Users List</h1>
		<p>Displays all the users.</p>
	</div>

@if (count($records) > 0)
    
    @if (Session::has('message'))
        <p class="alert alert-success"> {{ Session::get('message') }} </p>
    @endif

	<a href="{{ URL::to('admin/users/create') }}" class="btn btn-primary">Create a user</a> <br> <br>
    <table class="table table-striped">
      <thead>
        <tr>
          <th>#</th>
          <th>First Name</th>
          <th class="hidden-phone">Middle Name</th>
          <th class="hidden-phone">Last Name</th>
          <th class="hidden-phone">User Role</th>
          <th class="hidden-phone">Department</th>
          <th class="hidden-phone">Department Role</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
			@foreach ($records as $user)
		        <tr>
		          <td>{{ $user->id_user }}</td>
		          <td>{{ $user->first_name }} </td>
		          <td class="hidden-phone">{{ $user->middle_name }}</td>
		          <td class="hidden-phone">{{ $user->last_name }}</td>
		          <td>@if ( $user->id_role == 1 ) admin @else member @endif </td>
		          <td>{{ $user->department_name }}</td>
		          <td>{{ $user->department_role_name }}</td>
		          <td class="hidden-phone">
		          	@if ( $user->status == 1 )
		          		<a title="Enabled" href="javascript: void(0)" class="btn btn-success"><i class="icon-ok icon-white"></i></a>
		          	@else
		          		<a title="Disabled" href="javascript: void(0)" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
		          	@endif
		          </td>		          
		          <td>
		          	<a title="View User" class="btn btn-info" href="{{ URL::to("admin/users/view/$user->id_user") }}"><i class="icon-search icon-white"></i></a>
		          	<a title="Edit User" class="btn btn-primary" href="{{ URL::to("admin/users/update/$user->id_user") }}"><i class="icon-pencil icon-white"></i></a>
		          	<a title="Delete User" class="btn btn-danger" href="{{ URL::to("admin/users/delete/$user->id_user") }}"><i class="icon-remove icon-white"></i></a>
		          </td>		          
		        </tr>			
			@endforeach

      </tbody>
    </table>

    {{ $records->links(); }}
    
@else
	<p class="text-error">Sorry, no records found.</p>
@endif

	<a href="{{ URL::to('admin/users/create') }}" class="btn btn-primary pull-left">Create a user</a>

@stop