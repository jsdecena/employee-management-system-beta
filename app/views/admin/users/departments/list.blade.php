@extends('admin.tpl.master')

@section('title')
  User Departments - Employee Management and Leave System
@stop

@section('content')
	
		<div class="page-header">
			<h1>Departments</h1>			
		</div>
        
        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif		
		
		@if ( isset($records) && count($records) > 0 )
			<table class="table table-striped departments">
				<thead>
					<th>#</th>
					<th>Department Name</th>
					<th class="hidden-phone">Description</th>
					<th>Actions</th>
				</thead>
				<tbody>

					{{-- ITERATE THROUGH THE DEPARTMENT --}}
					@foreach ( $records as $record )
						<tr class="header">
							<td> {{ $record->id_department }} </td>
							<td> {{ $record->name }} </td>
							<td class="hidden-phone"> {{ $record->description }} </td>
							<td>								
								<a title="Add users" href="{{ URL::to("admin/users/departments/user_add/$record->id_department") }}" class="btn btn-info"><i class="icon-plus icon-white"></i> Add users </a>
								<a title="Edit this department" href="{{ URL::to("admin/users/departments/update/$record->id_department") }}" class="btn btn-primary"><i class="icon-pencil icon-white"></i></a>
								<a title="Delete this department" href="{{ URL::to("admin/users/departments/delete/$record->id_department") }}" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
							</td>
						</tr>
						
						{{-- ITERATE THE ROLES PER DEPARTMENT --}}
						@foreach ( $user_department_roles as $user_department_role )							
							
							{{-- CHECK IF THIS ROLE IS EXISTING IN THIS DEPARTMENT --}}
							@if ( $record->id_department == $user_department_role->id_department )
								<tr class="sub_header">
									<td> <span class="connect"></span> </td>
									<td> {{ $user_department_role->name }} </td>
									<td> {{ $user_department_role->description }} </td>
									<td class="hidden-phone"> &nbsp; </td>
								</tr>

								{{-- CHECK IF THIS USER IS EXISTING IN THIS ROLE --}} 
								@foreach ( $department_users as $department_user )

									@if ( $department_user->id_department_role == $user_department_role->id_department_role )
										<tr>
											<td><span class="connect level2"></span></td>
											<td>{{ User::find( $department_user->id_user )->last_name }}, {{ User::find( $department_user->id_user )->first_name }}</td>
											<td>{{ User::find( $department_user->id_user )->email }}</td>
											<td class="hidden-phone"> &nbsp; </td>
											<td>
												<a title="Edit this department" href="{{ URL::to("admin/users/departments/user_edit/$department_user->id_user/$department_user->id_department/$department_user->id_department_role") }}" class="btn btn-primary">Edit</a>
												<a title="Delete this department" href="{{ URL::to("admin/users/departments/user_delete/$department_user->id_user/$department_user->id_department/$department_user->id_department_role/$department_user->id_department_user") }}" class="btn btn-danger">Delete</a>												
											</td>
										</tr>
									@endif
								@endforeach
							@endif
						@endforeach
					@endforeach
				</tbody>
			</table>
		@else
			<p>Sorry, no data to show.</p>
		@endif

		<br /><a href="{{ URL::to('admin/users/departments/create') }}" class="btn btn-primary">Add a department</a>
@stop