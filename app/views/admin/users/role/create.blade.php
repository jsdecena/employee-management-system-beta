@extends('admin.tpl.master')

@section('title')
  Users Role Create - Employee Management and Leave System
@stop

@section('content')
	
	<div class="page-header">
		<h1>Users Role Create</h1>
		<p>Create a users role.</p>
	</div>

    @if ($errors->count() > 0)
     <p>The following errors have occurred:</p>
      <ul class="alert alert-error">
          {{ $errors->first('role', '<li>:message</li>') }}
      </ul> 
    @endif

    @if (Session::has('error'))
        <p class="alert alert-error"> {{ Session::get('error') }} </p>
    @elseif ( Session::has('success') )
      <p class="alert alert-success"> {{ Session::get('success') }} </p>
    @endif

	{{ Form::open(array('class' => 'vertical')) }}
		<div class="control-group">
			<label class="control-label" for="role">User Role Name</label>
			<div class="controls">
			  <input type="text" id="role" class="input-xlarge" name="role" value="" placeholder="Role name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="role_description">User Role Description</label>
			<div class="controls">
			  <textarea name="role_description" id="role_description" class="input-xlarge" cols="30" rows="5" placeholder="Role Description"></textarea>
			</div>
		</div>		
		<div class="control-group submit_button clear pull-left">
			<button class="btn btn-primary input-medium" name="user_role_create" id="user_role_create">Create a Role</button>
		</div>		  
	{{ Form::close() }}

@stop