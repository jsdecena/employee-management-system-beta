@extends('admin.tpl.master')

@section('title')
  Users Role Update - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>User Role Update</h1>
		<p>Update a user role.</p>
	</div>

	{{ Form::open(array('class' => 'vertical')) }}

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif
		
		<div class="control-group">
			<label class="control-label" for="role">User Role Name</label>
			<div class="controls">
			  <input type="text" id="role" class="input-xlarge" name="role" value="{{ $records->role }}" placeholder="Role name">
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="role_description">User Role Description</label>
			<div class="controls">
			  <textarea name="role_description" id="role_description" class="input-xlarge" cols="30" rows="5" placeholder="Role Description">{{ $records->role_description }}</textarea>
			</div>
		</div>		
		<div class="control-group submit_button clear pull-left">
			<a href="{{ URL::to('admin/users/role/list') }}" class="btn btn-inverse">Cancel</a>
			<button class="btn btn-primary input-medium" id="user_role_update">Update a Role</button>
		</div>		  
	{{ Form::close() }}

@stop