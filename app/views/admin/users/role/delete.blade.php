@extends('admin.tpl.master')

@section('title')
  Users Role Delete - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>User Role Delete</h1>
		<p>Confirm to delete this user role.</p>
	</div>

	{{-- START THE DELETE SECTION --}}
	
	{{ Form::open() }}

		<div class="control-group well">
			<div class="controls">
				<p>Are you sure you want to delete: {{ $records->role }}</p>
			</div>			
		</div>		

		<div class="control-group submit_button">
			<a href="{{ URL::to('admin/users/role/list') }}" class="btn btn-inverse">Cancel</a>
			<button class="btn btn-primary input-xlarge" name="user_delete" id="user_delete" value="Delete this user" onClick="return confirm('Confirm to delete this role?');">Delete</button>
		</div>
	{{ Form::close() }}	
@stop