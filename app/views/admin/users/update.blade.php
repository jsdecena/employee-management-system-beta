@extends('admin.tpl.master')

@section('title')
  Users Update - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>Users Update</h1>
		<p>Update a user.</p>
	</div>

	{{-- START THE UPDATE SECTION --}}
	
	{{ Form::open() }}

		<input type="hidden" name="id_user" value="{{ Request::segment(4) }}">
        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          	<p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif
        
        <div class="control-group well pull-left span8">
			<div class="pull-left">
				<h3>Personal Information</h3> <hr />
				<div class="controls btn-group">
					<label>Your Title</label>
					<div class="btn">
						<label for="title_mr">Mr.</label> <input type="radio" id="title_mr" name="title" value="Mr."@if ( $records->title == "Mr." ) checked="checked"@endif>
					</div>
					<div class="btn">
						<label for="title_ms">Ms.</label> <input type="radio" id="title_ms" name="title" value="Ms."@if ( $records->title == "Ms.") checked="checked"@endif>
					</div>
					<div class="btn">
						<label for="title_mrs">Mrs.</label> <input type="radio" id="title_mrs" name="title" value="Mrs."@if ( $records->title == "Mrs.") checked="checked"@endif>
					</div>	
				</div>		
				<div class="controls">
					<label for="first_name">First Name <sup class="text-error">*</sup></label>
					<input type="text" id="first_name" class="input-xlarge" name="first_name" value="{{ $records->first_name }}" placeholder="first name" />
				</div>
				<div class="controls">
					<label for="middle_name">Middle Name</label>
					<input type="text" id="middle_name" class="input-xlarge" name="middle_name" value="{{ $records->middle_name }}" placeholder="middle name" />
				</div>
				<div class="controls">
					<label for="last_name">Last Name</label>
					<input type="text" id="last_name" class="input-xlarge" name="last_name" value="{{ $records->last_name }}" placeholder="last name" />
				</div>
				<div class="controls">
					<label for="email">Email <sup class="text-error">*</sup></label>
					<input type="text" id="email" class="input-xlarge" name="email" value="{{ $records->email }}" placeholder="email" />				
				</div>		
				<div class="controls">
					<label for="mobile">Mobile</label>
					<input type="text" id="mobile" class="input-xlarge" name="mobile" value="{{ $records->mobile }}" placeholder="mobile phone" />
				</div>
				<div class="controls">
					<label for="tel">Tel.</label>
					<input type="text" id="tel" class="input-xlarge" name="tel" value="{{ $records->tel }}" placeholder="telephone" />
				</div>		
			</div>

			<div class="pull-right span4">
				<h3>Address</h3> <hr />
				<div class="controls">
					<label for="street">Street Address</label>
					<input type="text" id="street" class="input-xlarge" name="street" value="{{ $records->street }}" placeholder="street address" />
				</div>
				<div class="controls">
					<label for="town_city">Town / City </label>
					<input type="text" id="town_city" class="input-xlarge" name="town_city" value="{{ $records->town_city }}" placeholder="town or city" />
				</div>
				<div class="controls">
					<label for="postal">Postal / Zip code</label>
					<input type="text" id="postal" class="input-xlarge" name="postal" value="{{ $records->postal }}" placeholder="postal or zip code" />
				</div>
				<div class="controls">
					<label for="country">Country</label>
					<select id="country" name="country">
						<option value="AFG"@if ( $records->country == "AFG") selected="selected"@endif>Afghanistan</option>
						<option value="ALA"@if ( $records->country == "ALA") selected="selected"@endif>Aland Islands</option>
						<option value="ALB"@if ( $records->country == "ALB") selected="selected"@endif>Albania</option>
						<option value="DZA"@if ( $records->country == "DZA") selected="selected"@endif>Algeria</option>
						<option value="ASM"@if ( $records->country == "ASM") selected="selected"@endif>American Samoa</option>
						<option value="AND"@if ( $records->country == "AND") selected="selected"@endif>Andorra</option>
						<option value="AGO"@if ( $records->country == "AGO") selected="selected"@endif>Angola</option>
						<option value="AIA"@if ( $records->country == "AIA") selected="selected"@endif>Anguilla</option>
						<option value="ATA"@if ( $records->country == "ATA") selected="selected"@endif>Antarctica</option>
						<option value="ATG"@if ( $records->country == "ATG") selected="selected"@endif>Antigua and Barbuda</option>
						<option value="ARG"@if ( $records->country == "ARG") selected="selected"@endif>Argentina</option>
						<option value="ARM"@if ( $records->country == "ARM") selected="selected"@endif>Armenia</option>
						<option value="ABW"@if ( $records->country == "ABW") selected="selected"@endif>Aruba</option>
						<option value="AUS"@if ( $records->country == "AUS") selected="selected"@endif>Australia</option>
						<option value="AUT"@if ( $records->country == "AUT") selected="selected"@endif>Austria</option>
						<option value="AZE"@if ( $records->country == "AZE") selected="selected"@endif>Azerbaijan</option>
						<option value="BHS"@if ( $records->country == "BHS") selected="selected"@endif>Bahamas</option>
						<option value="BHR"@if ( $records->country == "BHR") selected="selected"@endif>Bahrain</option>
						<option value="BGD"@if ( $records->country == "BGD") selected="selected"@endif>Bangladesh</option>
						<option value="BRB"@if ( $records->country == "BRB") selected="selected"@endif>Barbados</option>
						<option value="BLR"@if ( $records->country == "BLR") selected="selected"@endif>Belarus</option>
						<option value="BEL"@if ( $records->country == "BEL") selected="selected"@endif>Belgium</option>
						<option value="BLZ"@if ( $records->country == "BLZ") selected="selected"@endif>Belize</option>
						<option value="BEN"@if ( $records->country == "BEN") selected="selected"@endif>Benin</option>
						<option value="BMU"@if ( $records->country == "BMU") selected="selected"@endif>Bermuda</option>
						<option value="BTN"@if ( $records->country == "BTN") selected="selected"@endif>Bhutan</option>
						<option value="BOL"@if ( $records->country == "BOL") selected="selected"@endif>Bolivia, Plurinational State of</option>
						<option value="BES"@if ( $records->country == "BES") selected="selected"@endif>Bonaire, Sint Eustatius and Saba</option>
						<option value="BIH"@if ( $records->country == "BIH") selected="selected"@endif>Bosnia and Herzegovina</option>
						<option value="BWA"@if ( $records->country == "BWA") selected="selected"@endif>Botswana</option>
						<option value="BVT"@if ( $records->country == "BVT") selected="selected"@endif>Bouvet Island</option>
						<option value="BRA"@if ( $records->country == "BRA") selected="selected"@endif>Brazil</option>
						<option value="IOT"@if ( $records->country == "IOT") selected="selected"@endif>British Indian Ocean Territory</option>
						<option value="BRN"@if ( $records->country == "BRN") selected="selected"@endif>Brunei Darussalam</option>
						<option value="BGR"@if ( $records->country == "BGR") selected="selected"@endif>Bulgaria</option>
						<option value="BFA"@if ( $records->country == "BFA") selected="selected"@endif>Burkina Faso</option>
						<option value="BDI"@if ( $records->country == "BDI") selected="selected"@endif>Burundi</option>
						<option value="KHM"@if ( $records->country == "KHM") selected="selected"@endif>Cambodia</option>
						<option value="CMR"@if ( $records->country == "CMR") selected="selected"@endif>Cameroon</option>
						<option value="CAN"@if ( $records->country == "CAN") selected="selected"@endif>Canada</option>
						<option value="CPV"@if ( $records->country == "CPV") selected="selected"@endif>Cape Verde</option>
						<option value="CYM"@if ( $records->country == "CYM") selected="selected"@endif>Cayman Islands</option>
						<option value="CAF"@if ( $records->country == "CAF") selected="selected"@endif>Central African Republic</option>
						<option value="TCD"@if ( $records->country == "TCD") selected="selected"@endif>Chad</option>
						<option value="CHL"@if ( $records->country == "CHL") selected="selected"@endif>Chile</option>
						<option value="CHN"@if ( $records->country == "CHN") selected="selected"@endif>China</option>
						<option value="CXR"@if ( $records->country == "CXR") selected="selected"@endif>Christmas Island</option>
						<option value="CCK"@if ( $records->country == "CCK") selected="selected"@endif>Cocos (Keeling) Islands</option>
						<option value="COL"@if ( $records->country == "COL") selected="selected"@endif>Colombia</option>
						<option value="COM"@if ( $records->country == "COM") selected="selected"@endif>Comoros</option>
						<option value="COG"@if ( $records->country == "COG") selected="selected"@endif>Congo</option>
						<option value="COD"@if ( $records->country == "COD") selected="selected"@endif>Congo, the Democratic Republic of the</option>
						<option value="COK"@if ( $records->country == "COK") selected="selected"@endif>Cook Islands</option>
						<option value="CRI"@if ( $records->country == "CRI") selected="selected"@endif>Costa Rica</option>
						<option value="CIV"@if ( $records->country == "CIV") selected="selected"@endif>Côte d'Ivoire</option>
						<option value="HRV"@if ( $records->country == "HRV") selected="selected"@endif>Croatia</option>
						<option value="CUB"@if ( $records->country == "CUB") selected="selected"@endif>Cuba</option>
						<option value="CUW"@if ( $records->country == "CUW") selected="selected"@endif>Curaçao</option>
						<option value="CYP"@if ( $records->country == "CYP") selected="selected"@endif>Cyprus</option>
						<option value="CZE"@if ( $records->country == "CZE") selected="selected"@endif>Czech Republic</option>
						<option value="DNK"@if ( $records->country == "DNK") selected="selected"@endif>Denmark</option>
						<option value="DJI"@if ( $records->country == "DJI") selected="selected"@endif>Djibouti</option>
						<option value="DMA"@if ( $records->country == "DMA") selected="selected"@endif>Dominica</option>
						<option value="DOM"@if ( $records->country == "DOM") selected="selected"@endif>Dominican Republic</option>
						<option value="ECU"@if ( $records->country == "ECU") selected="selected"@endif>Ecuador</option>
						<option value="EGY"@if ( $records->country == "EGY") selected="selected"@endif>Egypt</option>
						<option value="SLV"@if ( $records->country == "SLV") selected="selected"@endif>El Salvador</option>
						<option value="GNQ"@if ( $records->country == "GNQ") selected="selected"@endif>Equatorial Guinea</option>
						<option value="ERI"@if ( $records->country == "ERI") selected="selected"@endif>Eritrea</option>
						<option value="EST"@if ( $records->country == "EST") selected="selected"@endif>Estonia</option>
						<option value="ETH"@if ( $records->country == "ETH") selected="selected"@endif>Ethiopia</option>
						<option value="FLK"@if ( $records->country == "FLK") selected="selected"@endif>Falkland Islands (Malvinas)</option>
						<option value="FRO"@if ( $records->country == "FRO") selected="selected"@endif>Faroe Islands</option>
						<option value="FJI"@if ( $records->country == "FJI") selected="selected"@endif>Fiji</option>
						<option value="FIN"@if ( $records->country == "FIN") selected="selected"@endif>Finland</option>
						<option value="FRA"@if ( $records->country == "FRA") selected="selected"@endif>France</option>
						<option value="GUF"@if ( $records->country == "GUF") selected="selected"@endif>French Guiana</option>
						<option value="PYF"@if ( $records->country == "PYF") selected="selected"@endif>French Polynesia</option>
						<option value="ATF"@if ( $records->country == "ATF") selected="selected"@endif>French Southern Territories</option>
						<option value="GAB"@if ( $records->country == "GAB") selected="selected"@endif>Gabon</option>
						<option value="GMB"@if ( $records->country == "GMB") selected="selected"@endif>Gambia</option>
						<option value="GEO"@if ( $records->country == "GEO") selected="selected"@endif>Georgia</option>
						<option value="DEU"@if ( $records->country == "DEU") selected="selected"@endif>Germany</option>
						<option value="GHA"@if ( $records->country == "GHA") selected="selected"@endif>Ghana</option>
						<option value="GIB"@if ( $records->country == "GIB") selected="selected"@endif>Gibraltar</option>
						<option value="GRC"@if ( $records->country == "GRC") selected="selected"@endif>Greece</option>
						<option value="GRL"@if ( $records->country == "GRL") selected="selected"@endif>Greenland</option>
						<option value="GRD"@if ( $records->country == "GRD") selected="selected"@endif>Grenada</option>
						<option value="GLP"@if ( $records->country == "GLP") selected="selected"@endif>Guadeloupe</option>
						<option value="GUM"@if ( $records->country == "GUM") selected="selected"@endif>Guam</option>
						<option value="GTM"@if ( $records->country == "GTM") selected="selected"@endif>Guatemala</option>
						<option value="GGY"@if ( $records->country == "GGY") selected="selected"@endif>Guernsey</option>
						<option value="GIN"@if ( $records->country == "GIN") selected="selected"@endif>Guinea</option>
						<option value="GNB"@if ( $records->country == "GNB") selected="selected"@endif>Guinea-Bissau</option>
						<option value="GUY"@if ( $records->country == "GUY") selected="selected"@endif>Guyana</option>
						<option value="HTI"@if ( $records->country == "HTI") selected="selected"@endif>Haiti</option>
						<option value="HMD"@if ( $records->country == "HMD") selected="selected"@endif>Heard Island and McDonald Islands</option>
						<option value="VAT"@if ( $records->country == "VAT") selected="selected"@endif>Holy See (Vatican City State)</option>
						<option value="HND"@if ( $records->country == "HND") selected="selected"@endif>Honduras</option>
						<option value="HKG"@if ( $records->country == "HKG") selected="selected"@endif>Hong Kong</option>
						<option value="HUN"@if ( $records->country == "HUN") selected="selected"@endif>Hungary</option>
						<option value="ISL"@if ( $records->country == "ISL") selected="selected"@endif>Iceland</option>
						<option value="IND"@if ( $records->country == "IND") selected="selected"@endif>India</option>
						<option value="IDN"@if ( $records->country == "IDN") selected="selected"@endif>Indonesia</option>
						<option value="IRN"@if ( $records->country == "IRN") selected="selected"@endif>Iran, Islamic Republic of</option>
						<option value="IRQ"@if ( $records->country == "IRQ") selected="selected"@endif>Iraq</option>
						<option value="IRL"@if ( $records->country == "IRL") selected="selected"@endif>Ireland</option>
						<option value="IMN"@if ( $records->country == "IMN") selected="selected"@endif>Isle of Man</option>
						<option value="ISR"@if ( $records->country == "ISR") selected="selected"@endif>Israel</option>
						<option value="ITA"@if ( $records->country == "ITA") selected="selected"@endif>Italy</option>
						<option value="JAM"@if ( $records->country == "JAM") selected="selected"@endif>Jamaica</option>
						<option value="JPN"@if ( $records->country == "JPN") selected="selected"@endif>Japan</option>
						<option value="JEY"@if ( $records->country == "JEY") selected="selected"@endif>Jersey</option>
						<option value="JOR"@if ( $records->country == "JOR") selected="selected"@endif>Jordan</option>
						<option value="KAZ"@if ( $records->country == "KAZ") selected="selected"@endif>Kazakhstan</option>
						<option value="KEN"@if ( $records->country == "KEN") selected="selected"@endif>Kenya</option>
						<option value="KIR"@if ( $records->country == "KIR") selected="selected"@endif>Kiribati</option>
						<option value="PRK"@if ( $records->country == "PRK") selected="selected"@endif>Korea, Democratic People's Republic of</option>
						<option value="KOR"@if ( $records->country == "KOR") selected="selected"@endif>Korea, Republic of</option>
						<option value="KWT"@if ( $records->country == "KWT") selected="selected"@endif>Kuwait</option>
						<option value="KGZ"@if ( $records->country == "KGZ") selected="selected"@endif>Kyrgyzstan</option>
						<option value="LAO"@if ( $records->country == "LAO") selected="selected"@endif>Lao People's Democratic Republic</option>
						<option value="LVA"@if ( $records->country == "LVA") selected="selected"@endif>Latvia</option>
						<option value="LBN"@if ( $records->country == "LBN") selected="selected"@endif>Lebanon</option>
						<option value="LSO"@if ( $records->country == "LSO") selected="selected"@endif>Lesotho</option>
						<option value="LBR"@if ( $records->country == "LBR") selected="selected"@endif>Liberia</option>
						<option value="LBY"@if ( $records->country == "LBY") selected="selected"@endif>Libya</option>
						<option value="LIE"@if ( $records->country == "LIE") selected="selected"@endif>Liechtenstein</option>
						<option value="LTU"@if ( $records->country == "LTU") selected="selected"@endif>Lithuania</option>
						<option value="LUX"@if ( $records->country == "LUX") selected="selected"@endif>Luxembourg</option>
						<option value="MAC"@if ( $records->country == "MAC") selected="selected"@endif>Macao</option>
						<option value="MKD"@if ( $records->country == "MKD") selected="selected"@endif>Macedonia, the former Yugoslav Republic of</option>
						<option value="MDG"@if ( $records->country == "MDG") selected="selected"@endif>Madagascar</option>
						<option value="MWI"@if ( $records->country == "MWI") selected="selected"@endif>Malawi</option>
						<option value="MYS"@if ( $records->country == "MYS") selected="selected"@endif>Malaysia</option>
						<option value="MDV"@if ( $records->country == "MDV") selected="selected"@endif>Maldives</option>
						<option value="MLI"@if ( $records->country == "MLI") selected="selected"@endif>Mali</option>
						<option value="MLT"@if ( $records->country == "MLT") selected="selected"@endif>Malta</option>
						<option value="MHL"@if ( $records->country == "MHL") selected="selected"@endif>Marshall Islands</option>
						<option value="MTQ"@if ( $records->country == "MTQ") selected="selected"@endif>Martinique</option>
						<option value="MRT"@if ( $records->country == "MRT") selected="selected"@endif>Mauritania</option>
						<option value="MUS"@if ( $records->country == "MUS") selected="selected"@endif>Mauritius</option>
						<option value="MYT"@if ( $records->country == "MYT") selected="selected"@endif>Mayotte</option>
						<option value="MEX"@if ( $records->country == "MEX") selected="selected"@endif>Mexico</option>
						<option value="FSM"@if ( $records->country == "FSM") selected="selected"@endif>Micronesia, Federated States of</option>
						<option value="MDA"@if ( $records->country == "MDA") selected="selected"@endif>Moldova, Republic of</option>
						<option value="MCO"@if ( $records->country == "MCO") selected="selected"@endif>Monaco</option>
						<option value="MNG"@if ( $records->country == "MNG") selected="selected"@endif>Mongolia</option>
						<option value="MNE"@if ( $records->country == "MNE") selected="selected"@endif>Montenegro</option>
						<option value="MSR"@if ( $records->country == "MSR") selected="selected"@endif>Montserrat</option>
						<option value="MAR"@if ( $records->country == "MAR") selected="selected"@endif>Morocco</option>
						<option value="MOZ"@if ( $records->country == "MOZ") selected="selected"@endif>Mozambique</option>
						<option value="MMR"@if ( $records->country == "MMR") selected="selected"@endif>Myanmar</option>
						<option value="NAM"@if ( $records->country == "NAM") selected="selected"@endif>Namibia</option>
						<option value="NRU"@if ( $records->country == "NRU") selected="selected"@endif>Nauru</option>
						<option value="NPL"@if ( $records->country == "NPL") selected="selected"@endif>Nepal</option>
						<option value="NLD"@if ( $records->country == "NLD") selected="selected"@endif>Netherlands</option>
						<option value="NCL"@if ( $records->country == "NCL") selected="selected"@endif>New Caledonia</option>
						<option value="NZL"@if ( $records->country == "NZL") selected="selected"@endif>New Zealand</option>
						<option value="NIC"@if ( $records->country == "NIC") selected="selected"@endif>Nicaragua</option>
						<option value="NER"@if ( $records->country == "NER") selected="selected"@endif>Niger</option>
						<option value="NGA"@if ( $records->country == "NGA") selected="selected"@endif>Nigeria</option>
						<option value="NIU"@if ( $records->country == "NIU") selected="selected"@endif>Niue</option>
						<option value="NFK"@if ( $records->country == "NFK") selected="selected"@endif>Norfolk Island</option>
						<option value="MNP"@if ( $records->country == "MNP") selected="selected"@endif>Northern Mariana Islands</option>
						<option value="NOR"@if ( $records->country == "NOR") selected="selected"@endif>Norway</option>
						<option value="OMN"@if ( $records->country == "OMN") selected="selected"@endif>Oman</option>
						<option value="PAK"@if ( $records->country == "PAK") selected="selected"@endif>Pakistan</option>
						<option value="PLW"@if ( $records->country == "PLW") selected="selected"@endif>Palau</option>
						<option value="PSE"@if ( $records->country == "PSE") selected="selected"@endif>Palestinian Territory, Occupied</option>
						<option value="PAN"@if ( $records->country == "PAN") selected="selected"@endif>Panama</option>
						<option value="PNG"@if ( $records->country == "PNG") selected="selected"@endif>Papua New Guinea</option>
						<option value="PRY"@if ( $records->country == "PRY") selected="selected"@endif>Paraguay</option>
						<option value="PER"@if ( $records->country == "PER") selected="selected"@endif>Peru</option>
						<option value="PHL"@if ( $records->country == "PHL") selected="selected"@endif>Philippines</option>
						<option value="PCN"@if ( $records->country == "PCN") selected="selected"@endif>Pitcairn</option>
						<option value="POL"@if ( $records->country == "POL") selected="selected"@endif>Poland</option>
						<option value="PRT"@if ( $records->country == "PRT") selected="selected"@endif>Portugal</option>
						<option value="PRI"@if ( $records->country == "PRI") selected="selected"@endif>Puerto Rico</option>
						<option value="QAT"@if ( $records->country == "QAT") selected="selected"@endif>Qatar</option>
						<option value="REU"@if ( $records->country == "REU") selected="selected"@endif>Réunion</option>
						<option value="ROU"@if ( $records->country == "ROU") selected="selected"@endif>Romania</option>
						<option value="RUS"@if ( $records->country == "RUS") selected="selected"@endif>Russian Federation</option>
						<option value="RWA"@if ( $records->country == "RWA") selected="selected"@endif>Rwanda</option>
						<option value="BLM"@if ( $records->country == "BLM") selected="selected"@endif>Saint Barthélemy</option>
						<option value="SHN"@if ( $records->country == "SHN") selected="selected"@endif>Saint Helena, Ascension and Tristan da Cunha</option>
						<option value="KNA"@if ( $records->country == "KNA") selected="selected"@endif>Saint Kitts and Nevis</option>
						<option value="LCA"@if ( $records->country == "LCA") selected="selected"@endif>Saint Lucia</option>
						<option value="MAF"@if ( $records->country == "MAF") selected="selected"@endif>Saint Martin (French part)</option>
						<option value="SPM"@if ( $records->country == "SPM") selected="selected"@endif>Saint Pierre and Miquelon</option>
						<option value="VCT"@if ( $records->country == "VCT") selected="selected"@endif>Saint Vincent and the Grenadines</option>
						<option value="WSM"@if ( $records->country == "WSM") selected="selected"@endif>Samoa</option>
						<option value="SMR"@if ( $records->country == "SMR") selected="selected"@endif>San Marino</option>
						<option value="STP"@if ( $records->country == "STP") selected="selected"@endif>Sao Tome and Principe</option>
						<option value="SAU"@if ( $records->country == "SAU") selected="selected"@endif>Saudi Arabia</option>
						<option value="SEN"@if ( $records->country == "SEN") selected="selected"@endif>Senegal</option>
						<option value="SRB"@if ( $records->country == "SRB") selected="selected"@endif>Serbia</option>
						<option value="SYC"@if ( $records->country == "SYC") selected="selected"@endif>Seychelles</option>
						<option value="SLE"@if ( $records->country == "SLE") selected="selected"@endif>Sierra Leone</option>
						<option value="SGP"@if ( $records->country == "SGP") selected="selected"@endif>Singapore</option>
						<option value="SXM"@if ( $records->country == "SXM") selected="selected"@endif>Sint Maarten (Dutch part)</option>
						<option value="SVK"@if ( $records->country == "SVK") selected="selected"@endif>Slovakia</option>
						<option value="SVN"@if ( $records->country == "SVN") selected="selected"@endif>Slovenia</option>
						<option value="SLB"@if ( $records->country == "SLB") selected="selected"@endif>Solomon Islands</option>
						<option value="SOM"@if ( $records->country == "SOM") selected="selected"@endif>Somalia</option>
						<option value="ZAF"@if ( $records->country == "ZAF") selected="selected"@endif>South Africa</option>
						<option value="SGS"@if ( $records->country == "SGS") selected="selected"@endif>South Georgia and the South Sandwich Islands</option>
						<option value="SSD"@if ( $records->country == "SSD") selected="selected"@endif>South Sudan</option>
						<option value="ESP"@if ( $records->country == "ESP") selected="selected"@endif>Spain</option>
						<option value="LKA"@if ( $records->country == "LKA") selected="selected"@endif>Sri Lanka</option>
						<option value="SDN"@if ( $records->country == "SDN") selected="selected"@endif>Sudan</option>
						<option value="SUR"@if ( $records->country == "SUR") selected="selected"@endif>Suriname</option>
						<option value="SJM"@if ( $records->country == "SJM") selected="selected"@endif>Svalbard and Jan Mayen</option>
						<option value="SWZ"@if ( $records->country == "SWZ") selected="selected"@endif>Swaziland</option>
						<option value="SWE"@if ( $records->country == "SWE") selected="selected"@endif>Sweden</option>
						<option value="CHE"@if ( $records->country == "CHE") selected="selected"@endif>Switzerland</option>
						<option value="SYR"@if ( $records->country == "SYR") selected="selected"@endif>Syrian Arab Republic</option>
						<option value="TWN"@if ( $records->country == "TWN") selected="selected"@endif>Taiwan, Province of China</option>
						<option value="TJK"@if ( $records->country == "TJK") selected="selected"@endif>Tajikistan</option>
						<option value="TZA"@if ( $records->country == "TZA") selected="selected"@endif>Tanzania, United Republic of</option>
						<option value="THA"@if ( $records->country == "THA") selected="selected"@endif>Thailand</option>
						<option value="TLS"@if ( $records->country == "TLS") selected="selected"@endif>Timor-Leste</option>
						<option value="TGO"@if ( $records->country == "TGO") selected="selected"@endif>Togo</option>
						<option value="TKL"@if ( $records->country == "TKL") selected="selected"@endif>Tokelau</option>
						<option value="TON"@if ( $records->country == "TON") selected="selected"@endif>Tonga</option>
						<option value="TTO"@if ( $records->country == "TTO") selected="selected"@endif>Trinidad and Tobago</option>
						<option value="TUN"@if ( $records->country == "TUN") selected="selected"@endif>Tunisia</option>
						<option value="TUR"@if ( $records->country == "TUR") selected="selected"@endif>Turkey</option>
						<option value="TKM"@if ( $records->country == "TKM") selected="selected"@endif>Turkmenistan</option>
						<option value="TCA"@if ( $records->country == "TCA") selected="selected"@endif>Turks and Caicos Islands</option>
						<option value="TUV"@if ( $records->country == "TUV") selected="selected"@endif>Tuvalu</option>
						<option value="UGA"@if ( $records->country == "UGA") selected="selected"@endif>Uganda</option>
						<option value="UKR"@if ( $records->country == "UKR") selected="selected"@endif>Ukraine</option>
						<option value="ARE"@if ( $records->country == "ARE") selected="selected"@endif>United Arab Emirates</option>
						<option value="GBR"@if ( $records->country == "GBR") selected="selected"@endif>United Kingdom</option>
						<option value="USA"@if ( $records->country == "USA") selected="selected"@endif>United States</option>
						<option value="UMI"@if ( $records->country == "UMI") selected="selected"@endif>United States Minor Outlying Islands</option>
						<option value="URY"@if ( $records->country == "URY") selected="selected"@endif>Uruguay</option>
						<option value="UZB"@if ( $records->country == "UZB") selected="selected"@endif>Uzbekistan</option>
						<option value="VUT"@if ( $records->country == "VUT") selected="selected"@endif>Vanuatu</option>
						<option value="VEN"@if ( $records->country == "VEN") selected="selected"@endif>Venezuela, Bolivarian Republic of</option>
						<option value="VNM"@if ( $records->country == "VNM") selected="selected"@endif>Viet Nam</option>
						<option value="VGB"@if ( $records->country == "VGB") selected="selected"@endif>Virgin Islands, British</option>
						<option value="VIR"@if ( $records->country == "VIR") selected="selected"@endif>Virgin Islands, U.S.</option>
						<option value="WLF"@if ( $records->country == "WLF") selected="selected"@endif>Wallis and Futuna</option>
						<option value="ESH"@if ( $records->country == "ESH") selected="selected"@endif>Western Sahara</option>
						<option value="YEM"@if ( $records->country == "YEM") selected="selected"@endif>Yemen</option>
						<option value="ZMB"@if ( $records->country == "ZMB") selected="selected"@endif>Zambia</option>
						<option value="ZWE"@if ( $records->country == "ZWE") selected="selected"@endif>Zimbabwe</option>
					</select>						
				</div>								
			</div>
			<br class="clear" />
        </div>

		<hr class="clear" />

		<div class="control-group well pull-left span8">
			<div class="pull-left">
				<h3>Employment Details</h3> <hr />
				<div class="controls">
					<label for="company_name">Company Name</label>
					<input type="text" id="company_name" class="input-xlarge" name="company_name" value="{{ $records->company_name }}" placeholder="Company Name" />
				</div>
				<div class="controls">
					<label for="job_scope">Job Scope</label>
					<textarea name="job_scope" id="job_scope" class="input-xlarge" cols="90" rows="5" placeholder="Employees Job Scope">{{ $records->job_scope }}</textarea>
				</div>		
				<div class="controls">
					<label for="employment_date">Employment Date</label>
					<div class="input-append date" data-date="{{ $records->employment_date }}" data-date-format="yyyy-mm-dd">
					  <input name="employment_date" class="input-xlarge" size="16" type="text" value="{{ $records->employment_date }}" readonly>
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>				      					
				</div>
				<div class="controls">
					<label for="employment_date_end">Employment Date End</label>					
					<div class="input-append date" data-date="{{ $records->employment_date_end }}" data-date-format="yyyy-mm-dd">
					  <input name="employment_date_end" class="input-xlarge" type="text" value="{{ $records->employment_date_end }}" readonly>
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>					      					
				</div>				
				<div class="controls">
					<label for="salary">Salary</label>
					<input type="text" id="salary" class="input-xlarge" name="salary" value="{{ $records->salary }}" placeholder="salary" />
				</div>
				<div class="controls">
					<label for="currency">Currency</label>
					<input type="text" id="currency" class="input-xlarge" name="currency" value="{{ $records->currency }}" placeholder="currency" />
				</div>		
				<div class="controls">
					<label for="bank">Bank Name</label>
					<input type="text" id="bank" class="input-xlarge" name="bank" value="{{ $records->bank }}" placeholder="bank name" />
				</div>
				<div class="controls">
					<label for="bank">Bank Branch</label>
					<input type="text" id="bank_branch" class="input-xlarge" name="bank_branch" value="{{ $records->bank_branch }}" placeholder="bank branch" />
				</div>							
				<div class="controls">
					<label for="account_name">Account Name</label>
					<input type="text" id="account_name" class="input-xlarge" name="account_name" value="{{ $records->account_name }}" placeholder="account name" />
				</div>
				<div class="controls">
					<label for="account_number">Account Number</label>
					<input type="text" id="account_number" class="input-xlarge" name="account_number" value="{{ $records->account_number }}" placeholder="account number" />
				</div>
				<div class="controls">
					<label for="account_type">Account Type</label>
					<input type="text" id="account_type" class="input-xlarge" name="account_type" value="{{ $records->account_type }}" placeholder="account type" />
				</div>							
				<div class="controls">
					<label for="notes">Notes</label>
					<textarea name="others" id="notes" class="input-xlarge" cols="90" rows="5" placeholder="other information">{{ $records->notes }}</textarea>
				</div>						
			</div>			
			<div class="pull-right span4">
				<h3>Employee Leave Details</h3> <hr />
				<div class="controls">
					<label for="annual_leave">Annual Leaves</label>
					<input type="text" id="annual_leave" class="input-xlarge" name="annual_leave" value="{{ $records->annual_leave }}" placeholder="annual leaves" />
				</div>
				<div class="controls">
					<label for="sick_leave">Sick Leaves</label>
					<input type="text" id="sick_leave" class="input-xlarge" name="sick_leave" value="{{ $records->sick_leave }}" placeholder="sick / medical leaves" />
				</div>
				@if ( $records->title != "Mr.")
				<div class="controls">
					<label for="maternity_leave">Maternity Leaves</label>
					<input type="text" id="maternity_leave" class="input-xlarge" name="maternity_leave" value="{{ $records->maternity_leave }}" placeholder="maternity leaves" />
				</div>
				@endif

				@if ( $records->title != "Ms." && $records->title != "Mrs.")
				<div class="controls">
					<label for="paternity_leave">Paternity Leaves</label>
					<input type="text" id="paternity_leave" class="input-xlarge" name="paternity_leave" value="{{ $records->paternity_leave }}" placeholder="paternity leaves" />
				</div>
				@endif			
			</div>
			<br class="clear" />		
		</div>

		<div class="control-group well pull-left span8">
			<?php if ( Session::get('user_role') == 1 ) : ?>
				<div id="update_role" class="controls">
					<label for="id_role">Role</label>
					<select name="id_role" id="id_role">
						@foreach ($roles as $role)
						    <option value="{{ $role['id_role'] }}">{{ $role['role'] }}</option>
						@endforeach
					</select>
				</div>					
			<?php endif; ?>

			<div class="controls btn-group">
				<label>Status</label>
				<div class="btn">
					<label for="status_on">Enable</label> <input type="radio" id="status_on" name="status" value="1"@if ( $records->status == 1) checked="checked"@endif>
				</div>
				<div class="btn">
					<label for="status_off">Disable</label> <input type="radio" id="status_off" name="status" value="0"@if ( $records->status == 0) checked="checked"@endif>
				</div>	
			</div>
			<div class="controls">
				<label>Change Password</label>
				<a href="{{ URL::to('admin/users/update/password') }}/{{ $records->id_user }}" class="btn btn-info">Change password</a>
			</div>			
		</div>		

		<div class="control-group submit_button span8">
			<a href="{{ URL::to('admin/users/list') }}" class="btn btn-inverse">Cancel</a>
			<button class="btn btn-primary input-xlarge" id="user_update" onClick="return confirm('Update this user?');">Update</button>
		</div>
	{{ Form::close() }}	
@stop