@extends('admin.tpl.master')

@section('title')
  Leave history - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>Leave History</h1>
		<p>Details of leaves.</p>
	</div>
	
	@if ( isset($records) && count($records) > 0 )
	    <table class="table table-striped leave_history">
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>Employee Name</th>
	          <th class="hidden-phone">Leave Type</th>
	          <th class="hidden-phone">Day Leave Type</th>
	          <th class="span3 hidden-phone">Dates</th>
	          <th>Number of day/s</th>
	          <th class="hidden-phone">Day Session</th>
	          <th class="hidden-phone">Other reason</th>
			  <th class="hidden-phone">Leave details</th>
				@if ( Session::get('user_role') == 1 )
			  		<th class="span2">Actions</th>
			  	@endif
	          <th>Request Status</th>
	          <th class="hidden-phone">Authorized by </th>
	          <th class="hidden-phone">Approved Date</th>
	          <th class="hidden-phone">Notes</th>
	        </tr>
	      </thead>		
	      <tbody>
			@foreach ( $records as $record )      	
		      	<tr>
		      		<td> {{ $record->id_leave }} </td>
		      		<td> {{ $record->last_name }}, {{ $record->first_name }}</td>
		      		<td class="hidden-phone">{{ $record->leave_type }}</td>
		      		<td class="hidden-phone">{{ $record->day_leave_type }}</td>
		      		<td class="span3 hidden-phone">
		      			@if( $record->day_leave_type == "halfday" || $record->day_leave_type == "oneday" )
		      				{{ date("D - F j, Y", strtotime( $record->day_leave )) }}
		      			@else
							Start: {{ date("D - F j, Y", strtotime( $record->date_from )) }} <br /> 
		      				End: {{ date("D - F j, Y", strtotime( $record->date_to )) }}
		      			@endif
		      		</td>
		      		<td>
						{{ $record->number_of_days }}
		      		</td>
		      		<td class="hidden-phone">
		      			@if( $record->day_leave_type == "halfday" )
		      				{{ $record->leave_time }}
		      			@else
		      				-
		      			@endif
		      		</td>
		      		<td class="hidden-phone">
		      			@if ( $record->other_reason )
		      				{{ $record->other_reason }}
		      			@else
		      				-
		      			@endif
		      		</td>
		      		<td class="hidden-phone">
		      			@if ( isset($record->leave_details) )		      				
		      				<button type="button" data-toggle="modal" data-target="#leave_details_{{ $record->id_leave}}" class="btn btn-primary">details</button>
							<div id="leave_details_{{ $record->id_leave}}" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-header">
							    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							    <h3 id="myModalLabel">Leave Details</h3>
							  </div>
							  <div class="modal-body">
							    <p>{{ $record->leave_details }}</p>
							  </div>
							  <div class="modal-footer">
							    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							  </div>
							</div>		      				
		      			@else
		      				-
		      			@endif
		      		</td>	      		
		      		<td>
		      			@if ( $record->approval == 0 )
		      				<span class="btn btn-warning">  Pending</i></span>
		      			@elseif ( $record->approval == 1 )
		      				<span class="btn btn-success"> Approved</span>
		      			@else
		      				<span class="btn btn-danger"> Denied</span>
		      			@endif
		      		</td>
		      		<td class="span2">
		      			<a title="View User's Leave" class="btn btn-info" href="{{ URL::to("admin/users/leave/history/$record->id_user") }}"><i class="icon-search icon-white"></i></a>
		      			<a title="Edit User's Leave" class="btn btn-primary" href="{{ URL::to("admin/users/leave/update/$record->id_leave") }}"><i class="icon-pencil icon-white"></i></a>
		      		</td>		      		
		      		<td class="hidden-phone">
		      			@if ( isset($record->approved_by) )
		      				{{ $record->approved_by }}
		      			@else
		      				-
		      			@endif
		      		</td>
		      		<td class="hidden-phone">
		      			@if ( isset($record->approved_date) && $record->approved_date != "0000-00-00")
		      				{{ date("F j, Y", strtotime($record->approved_date)) }}
		      			@else
		      				-
		      			@endif
		      		</td>
		      		<td class="hidden-phone">
		      			@if ( $record->notes )
		      				<button type="button" data-toggle="modal" data-target="#notes" class="btn btn-primary">details</button>
							<div id="notes" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-header">
							    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
							    <h3 id="myModalLabel">Leave Details</h3>
							  </div>
							  <div class="modal-body">
							    <p>{{ $record->notes }}</p>
							  </div>
							  <div class="modal-footer">
							    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
							  </div>
							</div>		      				
		      			@else
		      				-
		      			@endif
		      		</td>
		      	</tr>
			@endforeach
	      <tbody>
	    </table>
	@else
		<p>Sorry, no data to show.</p>
	@endif    
@stop