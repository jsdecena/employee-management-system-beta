@extends('admin.tpl.master')

@section('title')
  Leave Update - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>Leave Update</h1>
		<p>Update leave request.</p>
	</div>

	@foreach ( $records as $record )

	{{ Form::open(array('class' => 'vertical')) }}
		
		{{ Form::hidden('id_user', $record->id_user ) }}
		{{ Form::hidden('email', Session::get('email') ) }}
		{{ Form::hidden('first_name', $record->first_name ) }}
		{{ Form::hidden('last_name', $record->last_name ) }}
		{{ Form::hidden('approved_by', Session::get('last_name') .', '. Session::get('first_name') ) }}
		{{ Form::hidden('approved_date', date("Y-m-d") ) }}

        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('leave_details', '<li>:message</li>') }}
              {{ $errors->first('approval', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif

        <div id="alert" class="alert alert-error" style="display:none">
        	<strong></strong>
        </div>		

			<div class="control-group span8">
				<label for="first_name">First Name</label>
				<div class="controls">
					<input type="text" value="{{ $record->first_name }}" id="first_name" class="input-xlarge" readonly>
				</div>
			</div>

			<div class="control-group span8">
				<label for="last_name">Last Name</label>
				<div class="controls">
					<input type="text" value="{{ $record->last_name }}" id="last_name" class="input-xlarge" readonly>
				</div>
			</div>			

			<div class="control-group span8">
				<label for="leave_type">Type of Leave</label>
				<div class="controls">
					{{ Form::select('leave_type', array('annual' => 'Annual / Vacation Leave', 'sick' => 'Sick / Medical Leave', 'other' => 'Other'), $record->leave_type, array('class' => 'input-xlarge', 'id' => 'leave_type')) }}
				</div>
				@if ( isset($record->other_reason) )				
				<div class="controls other_reason">
					<label for="other_reason">Other Reason</label>
					{{ Form::text('other_reason', Input::old('other_reason'), array('id' => 'other_reason', 'class' => 'input-xlarge', 'placeholder' => 'Other Reason', 'rows' => '5') ) }}				
				</div>
				@endif
			</div>

			<div class="control-group span8">
				<label for="day_leave_type" class="control-label">Day Leave Type</label>
				<div class="controls">
					{{ Form::select('day_leave_type', array('#noSelect' => '--Select--', 'halfday' => 'Half Day', 'oneday' => 'One Day', 'multiple' => 'Multiple days'), $record->day_leave_type, array('id' => 'day_leave_type', 'class' => 'input-xlarge') ) }}
				</div>
			</div>

			@if ( $record->day_leave_type == "halfday" )
				<div id="leave_time_wrap" class="control-group span8">
					<label for="leave_time" class="control-label">Time of the day</label>
					<div class="controls">
						{{ Form::select('leave_time', array('#noSelect' => '--Select--','0' => 'Morning', '1' => 'Afternoon'), $record->leave_time, array('id' => 'leave_time', 'class' => 'input-xlarge') ) }}
					</div>
				</div>
			@endif

			@if ( $record->day_leave_type == "multiple")
			<div id="date_picker_wrap" class="control-group span8">
				<fieldset class="pull-left">
					<label class="control-label" for="date_from">Date From:</label>
					<div class="controls">
						<div class="input-append date" data-date="{{ date($record->date_from) }}" data-date-format="yyyy-mm-dd">
						  <input name="date_from" id="date_from" class="input-xlarge" size="16" type="text" value="{{ date($record->date_from) }}">					  
						  <span class="add-on"><i class="icon-th"></i></span>
						</div>				      					
					</div>
				</fieldset>

				<fieldset class="pull-left span3">
					<label class="control-label" for="date_to">Date To:</label>
					<div class="controls">
						<div class="input-append date" data-date="{{ date($record->date_to) }}" data-date-format="yyyy-mm-dd">
						  <input name="date_to" id="date_to" class="input-xlarge" size="16" type="text" value="{{ date($record->date_to) }}">
						  <span class="add-on"><i class="icon-th"></i></span>
						</div>				      					
					</div>
				</fieldset>
			</div>
			@elseif ( $record->day_leave_type == "halfday" || $record->day_leave_type == "oneday" )
				<div id="single_day" class="control-group span8">
					<fieldset class="pull-left">
						<label class="control-label">Date</label>
						<div class="controls">
							<div class="input-append date" data-date="{{ date($record->day_leave) }}" data-date-format="yyyy-mm-dd">
							  <input name="day_leave" id="day_leave" class="input-xlarge" size="16" type="text" value="{{ date($record->day_leave) }}" disabled>			  
							  <span class="add-on"><i class="icon-th"></i></span>
							</div>				      					
						</div>
					</fieldset>
				</div>			
			@endif

			<div class="control-group span8">
				<label for="number_of_days" class="control-label">Number of days</label>
				<div class="controls">
					<input type="text" value="{{ $record->number_of_days}}" id="number_of_days" class="input-xlarge" readonly>
				</div>
			</div>

			<div class="control-group span8">
				<label for="leave_details">Details of the leave <sup class="text-error">*</sup></label>
				<div class="controls">
					{{ Form::textarea('leave_details', $record->leave_details, array('id' => 'leave_details', 'class' => 'input-xlarge', 'placeholder' => 'Details'))}}
				</div>
			</div>					

		<div class="control-group clear span8">
			<label for="actions">Approval Actions <sup class="text-error">*</sup></label>
				<div class="controls">
					<div class="btn-group btn span2">
						<label for="approved_yes">Approve</label>
						<input type="radio" name="approval" value="1" id="approved_yes"@if( $record->approval == 1) checked="checked" @endif/>
					</div>					
					<div class="btn-group btn span2">
						<label for="approved_no">Deny</label>
						<input type="radio" name="approval" value="2" id="approved_no"@if( $record->approval == 2) checked="checked" @endif/>
					</div>
				</div>
		</div>
		
		<div class="control-group span8"> <br />
			<label for="denial_reason" class="control-label">If denied, reason for denial</label>
			<div class="controls">
				{{ Form::textarea('notes', Input::old('notes'), array('id' => 'notes', 'class' => 'input-xlarge', 'placeholder' => 'Give reason for leave denial') )}}
			</div>
		</div>

		<div class="control-group clear span8">
			<div class="controls btn-group">				
				<button class="btn btn-primary" onClick="return confirm('Update this leave request?');">Submit</button>
			</div>
		</div>

	{{ Form::close() }}
	
	@endforeach
@stop