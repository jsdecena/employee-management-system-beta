<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Forgot password - Employee Management System </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="{{ URL::asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="{{ URL::asset('assets/css/bootstrap-responsive.css') }}" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{ URL::asset('assets/ico/favicon.ico') }}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ URL::asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">
  </head>

  <body>  

    <div class="container">

      {{ Form::open(array('class' => 'form-signin')) }}
        <h3 class="form-signin-heading">Retrieve your Password</h3>
        
        @if (Session::has('error'))
            <p class="alert alert-error">{{ trans(Session::get('error')) }}</p>
        @elseif (Session::has('success'))
            <p class="alert alert-success">An e-mail with the password reset has been sent.</p>
        @endif

        {{ Form::text('email', Input::old('email'), array('id' => 'email', 'class' => 'input-block-level', 'placeholder' => 'Your email address')) }}

        <button class="btn btn-large btn-primary" type="submit">get it now</button>
        <br class="clear">
      
      {{ Form::close() }}

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ URL::asset('assets/js/jquery.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-transition.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-alert.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-modal.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-dropdown.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-scrollspy.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-tab.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-tooltip.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-popover.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-button.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-collapse.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-carousel.js') }}"></script>
    <script src="{{ URL::asset('assets/js/bootstrap-typeahead.js') }}"></script>

  </body>
</html>