<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>		
		<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable">
		    <tr>
		        <td align="center" valign="top">
		            <table border="0" cellpadding="20" cellspacing="0" width="600" id="emailContainer">
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailBody">
		                            <tr>
		                                <td align="left" valign="top">
		                                   <h2>Employee Management &amp; Leave System</h2>
		                                </td>
		                            </tr>		                        	
		                            <tr>
		                                <td align="left" valign="top">
											<h4>Please review your request below:</h4><br />	                                	
											
											Name: {{ $first_name . $last_name }} <br />
											Type of leave used: {{ $leave_type }} <br />
											@if ( $other_reason )
												Other reason: {{ $other_reason }} <br />
											@endif
											
											@if ( $day_leave_type == "halfday" )
												Leave Time : {{ $leave_time }} <br />
											@endif
											
											@if ( $day_leave_type == "multiple" )
												Request Date from: {{ date("F j, Y", strtotime($date_from)) }}<br />
												Request Date To: {{ date("F j, Y", strtotime($date_to)) }}<br />
											@else
												Request Date: {{ date("F j, Y", strtotime($day_leave)) }} <br />
											@endif
											
											Total days on leave: {{ $number_of_days }} <br />
											Leave details: {{ $leave_details }} <br /><br />
											Status: <strong style="color:green">Disapproved</strong> <br />
											Denial Reason: {{ $notes }} <br /> <br />

											Authorized by: {{ $approved_by }}
											Authorized date: {{ $approved_date }}											
		                                </td>
		                            </tr>		                            
		                        </table>
		                    </td>
		                </tr>
		                <tr>
		                    <td align="center" valign="top">
		                        <table border="0" cellpadding="20" cellspacing="0" width="100%" id="emailFooter">
		                            <tr>
		                                <td align="left" valign="top">
		                                   &copy; 2013 Employee Management &amp; Leave System | Built by <a href="http://jsdecena.me">JSDecena</a>
		                                </td>
		                            </tr>
		                        </table>
		                    </td>
		                </tr>
		            </table>
		        </td>
		    </tr>
		</table>		
	</body>
</html>