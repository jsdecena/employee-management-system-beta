<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentRoles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('department_roles', function(Blueprint $table)
		{
			$table->increments('id_department_role');			
			$table->integer('id_department')->unsigned();
			$table->foreign('id_department')->references('id_department')->on('departments');
			$table->string('name');
			$table->text('description');			
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('department_roles');
	}

}