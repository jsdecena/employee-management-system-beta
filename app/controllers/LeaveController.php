<?php

class LeaveController extends BaseController {

	public function index()
	{
		$records 	= User::find( Session::get('id_user') );

		return View::make('admin/users/leave/create')->with('records', $records);
	}

	public function leaveList()
	{
		$records = UserLeave::orderBy('id_leave', 'desc')->paginate(10);

		return View::make('admin/users/leave/list')->with('records', $records);
	}

	public function leaveUpdate()
	{
		$records['leave'] = UserLeave::where('id_leave', '=', Request::segment(5))->first();

		return View::make('admin/users/leave/update')->with('records', $records);
	}		

	public function history()
	{		
		$records = UserLeave::where('id_user', '=', Session::get('id_user'))->get();
		
		return View::make('admin/users/leave/history')->with('records', $records);
	}

	public function historyUser()
	{
		$records = UserLeave::orderBy('id_leave', 'desc')->where('id_user', '=', Request::segment(5))->get();

		return View::make('admin/users/leave/history')->with('records', $records);
	}	
}