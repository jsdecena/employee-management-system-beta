<?php

class AdminController extends BaseController {

	public function index()
	{
		return View::make('signin');
	}

	public function dashboard() {
		
		$records = Auth::user();

		return View::make('admin/admin')->with('records', $records);
	}	

	public function logout(){
		
		Auth::logout();

		return Redirect::to('/')->with('message', 'You have successfully logged out of the system');
	}

	public function settings(){

		$records 	= Departments::all();
		$settings 	= Settings::all();

		return View::make('admin/settings/list')
			->with('records', $records)
			->with('settings', $settings);

	}

}