## EMPLOYEE MANAGEMENT SYSTEM

EMPLOYEE MANAGEMENT SYSTEM is a Laravel based system for small companies to properly manage employee details and track leave requests.
This is also made with twitter bootstrap to make it multiple device responsive for better mobile devices access which you can check your employee details and leave requests straight from your phone/tablet.

###HOW TO INSTALL
1. Dump all the files in your server's public folder.
2. Create a database in your server then settings in your app/config/database.
3. SSH to your server, go to this folder's root directory and run "php artisan migrate --seed" and your database will automatically populated.
4. Default user detail is: email: john@doe.com / password: Testing123 -- This is the admin account.

###SETTINGS
1. You must set the "Leave Email" in the Settings. This email will receive all the leave requests. Usually this detail is the company admin staff's email.


> EMPLOYEE MANAGEMENT SYSTEM
Copyright (C) <2013>  <jeff@jsdecena.me>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
